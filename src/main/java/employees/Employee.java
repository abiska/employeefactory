package employees;


public class Employee {
    
    //attribute(s)
    protected String name;
    protected double hourlyWage;
    protected double hoursWorked;
    
    //constructor(s)
    public Employee(String name, double hourlyWage, double hoursWorked){
    this.name = name;
    this.hourlyWage = hourlyWage;
    this.hoursWorked = hoursWorked;
    }
    
    //getter(s)
    public String getName() {
        return name;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }
    
    //Method(s)
    public double getHoursWorked() {
        return hoursWorked;
    }
    
    //better than stdout (high cohesion, lose coupling)
    public double calculatePay(){
        return getHourlyWage()*getHoursWorked();
    }
}