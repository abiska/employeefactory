package employees;


public class PayrollSimulation {


    public static void main(String[] args) {

        //instance(s)
        EmployeeFactory factory = EmployeeFactory.getInstance();
        
        Employee Manager1 = factory.getEmployee(EmployeeEnum.MANAGER, "Jerry", 50, 60, 70);
        Employee Employee1 = factory.getEmployee(EmployeeEnum.EMPLOYEE, "Tom", 10, 20, 0);
    
        
        System.out.println( Employee1.getName() + "'s paycheck = $" + Employee1.calculatePay());
        
        System.out.println( Manager1.getName() + "'s paycheck = $" + Manager1.calculatePay() );
        
    }
}
