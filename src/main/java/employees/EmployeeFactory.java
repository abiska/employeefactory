package employees;

/**
 *
 * @author iuabd
 */
public class EmployeeFactory {
    
    private static EmployeeFactory factory; //private object
    
    private EmployeeFactory(){} //private default constructor
    
    public static EmployeeFactory getInstance() //the only public method that allows 1 instanciation at a time
    {
        if(factory==null)
            factory = new EmployeeFactory();
        
        return factory;
    }
    
    public Employee getEmployee(EmployeeEnum type, String name, double hourlyWage, double hoursWorked, double bonus){
    
        switch(type){
                case MANAGER: return new Manager(name, hourlyWage, hoursWorked, bonus);
                case EMPLOYEE: return new Employee(name, hourlyWage, hoursWorked);
        }
        return null;
    
    }   
}