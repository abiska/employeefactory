package employees;


public class Manager extends Employee {
    
    //attribute(s)
    private double bonus;
    
    //constructor(s)
    public Manager(String name, double hourlyWage, double hoursWorked, double bonus){
        super(name, hourlyWage, hoursWorked);
        this.bonus = bonus;
    }
    
    //getter(s)
    public double getBonus() {
        return bonus;
    }
    
    //Method(s)
    @Override
    public double calculatePay(){
        return super.calculatePay()+getBonus();
    }
}
